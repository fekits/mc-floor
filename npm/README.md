# MC-FLOOR
```$xslt
一个楼层监听插件，当楼层滚动到可见区域的指定位置时触发回调，可用于楼层绑定导航等场景。

1. 页面滚动到指定楼层出现在可视区时触发事件并返回相关回调信息。
2. 可以监听当前页面的滚动操作是向上还是向下，滚动到顶部或底部并返回滚动条的位置信息。
3. 可以设置不同的入场或出场位置，即指定楼层滚动到可视区时超过指定的位置时才触发事件。
4. 是否设置让插件仅监听一次，再次滚动页面时触发过事件的楼层不再触发事件。
5. 可以在插件初始化后临时添加监听楼层和删除监听楼层。
6. 无任何监听楼层在可视区时是否保留最近的监听对象为当前楼层-
```

#### 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)
* [反馈](#反馈)


#### 演示
[https://junbo.name/plugins/mc-floor](https://junbo.name/plugins/mc-floor/)


#### 开始

下载项目: 

```npm
npm i mc-floor
```

#### 参数
```$xslt
param               {Object}    入参为一个对象

param.el            {String}    DOM节点选择器，可以是id,class,name,data自定义属性等等
param.topOffset     {Number}    顶部偏移量值,可以填入两种类型的数字。1.直接写0,1及以上的数字,比如滚动到100px时触发就是写100。2.写大于0小于1的小数会转化为百分比,比如滚动到屏幕一半时触发就是0.5
param.bottomOffset  {Number}    底部偏移量值,可以填入两种类型的数字。1.直接写0,1及以上的数字,比如滚动到100px时触发就是写100。2.写大于0小于1的小数会转化为百分比,比如滚动到屏幕一半时触发就是0.5
param.keepActive    {Boolean}   当所有楼层都不在可视区时，是否开启至少保持一个当前楼层？用于楼层与导航绑定，导航至少要有一个保持当前导航的情况
param.once          {Boolean}   是否仅执行一次,默认不fals，once为true时，每个楼层只爆光一次;当开启once为true时keepActive保持失效（因为保持离可视区最近的楼层为当前楼层不可用）。
param.on            {Object}    回调事件集
param.on.up         {Function}  当向上滚动时触发
param.on.down       {Function}  当向下滚动时触发
param.on.top        {Function}  滚动到顶部时触发
param.on.down       {Function}  滚动到底部时触发
param.on.change     {Function}  楼层切换时触发
```

#### 示例

```javascript
// 引入插件
import McFloor from 'mc-floor';

// 示例用的跟楼层关联的导航
let navsBox = document.getElementById('floor_nav');
let navs = navsBox.children;

// 获取全部的楼层
let aFloors = document.querySelectorAll('.track_floor');

// 监听楼层
new McFloor({
  el: aFloors,
  topOffset: 10,
  bottomOffset: .5,
  // keepActive:true,
  // once: true,
  on: {
    init() {
      // 这是初始化做一些事，这个只触发一次哦！
    },
    down() {
      // 向下滚动时做一些事...
    },
    up() {
      // 滚动到页面顶部时部时可以在这里做一些事...
    },
    end() {
      // 滚动到页面底部时可以在这里做一些事...
    },
    top() {
      // 向上滚动时做一些事...
    },
    change(res) {
      // 切换了一个楼层啦
      
      // 所有的导航清掉"active" class;
      let navsLength = navs.length;
      for (let i = 0; i < navsLength; i++) {
        navs[i].className = '';
      }

      // 给当前楼层对应的导航加上"active" class;
      if (res.activeFloor) {
        console.log(res.activeFloor);
        navs[res.activeIndex].className = 'active';
      }
    }
  }
});

// 其实你也可以直接填入选择器
new McFloor({
  el: '.track_floor'
});

// 实例的其它功能函数
let myFloor = new McFloor({el:'.xxx'});
// 为实例新增一个楼层
myFloor.add(document.getElementById('xxx'));
// 为实例删除一个楼层
myFloor.del(document.getElementById('xxx'));
// 销毁实例
myFloor.destroy();
```

#### 版本

```$xslt
v1.0.8 [Latest version]
1. 新增外部设置的排队标识free,当外部正在进行任务需要暂停楼层监听时使用
```

```$xslt
v1.0.7
1. 为新增与删除楼层增加回调事件
```

```$xslt
v1.0.6
1. 新增销毁函数destroy()
2. 新增一个楼层add(el)
3. 删除一个楼层del(el)
```

```$xslt
v1.0.5
1. 新增一个入参控制选项once，是否仅执行一次,默认不false，once为true时，每个楼层只爆光一次。每个楼层的change只执行一次。当开启once为true时keepActive保持失效（因为保持离可视区最近的楼层为当前楼层不可用）。
2. 修复一个直接引用插件时挂载在window下的插件名错识。
```

```$xslt
v1.0.4
1. 重写内核，以getBoundingClientRect实现
2. 移除then这个回调，所有回调放入on回调事件集中。新增了init回调和change回调。
3. 新增了add功能和del功能用于new了一个实例后又临时增减实例的楼层
4. 移除了offset入参，改为topOffset和bottomOffset并提供了绝对数值和百分比两种入参形式。
5. 新增了keepActive入参，此开关决定当所有楼层不在可视区时是否保持离可视区最近的楼层为当前楼层。
6. el入参修改为可以采用querySelectorAll能识别的所有选择器及直接入参为HTMLElement节点元素或一组节点元素集。
```

```$xslt
v1.0.3
1. 修复requestAnimationFrame兼容性
2. 非核心更新，整理文档
```

```$xslt
v1.0.2
1. 新增了on回调事件集。可以监听滚动到顶部，洋动到底部，滚动方向等
   on.up()   向上滚动时触发
   on.down() 向下滚动时触发
   on.top()  滚动到顶部时触发
   on.end()  滚动到底部时触发
```

```$xslt
v1.0.0
1. 核心功能完成。
```

#### 反馈
```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
