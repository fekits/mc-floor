import '../css/demo.scss';

// 代码着色插件
import McTinting from 'mc-tinting';

new McTinting();

// 转换为DOM
function toDom(str) {
  let div = document.createElement('div');
  if (typeof str == 'string')
    div.innerHTML = str;
  return div.children[0];
}

// 监听楼层插件
import McFloor from '../../lib/mc-floor';

// window.addEventListener('load', () => {
//
//
// });

let navsBox = document.getElementById('floor_nav');
let navs = navsBox.children;

let aFloors = document.querySelectorAll('.track_floor');

// 监听楼层
let myFloor = new McFloor({
  el: aFloors,
  topOffset: 10,
  bottomOffset: .5,
  // keepActive:true,
  // once: true,
  on: {
    init() {
      // 这是初始化做一些事，这个只触发一次哦！
    },
    down() {
      // 向下滚动时做一些事...
    },
    up() {
      // 滚动到页面顶部时部时可以在这里做一些事...
    },
    end() {
      // 滚动到页面底部时可以在这里做一些事...
    },
    top() {
      // 向上滚动时做一些事...
    },
    change(res) {
      console.log('现在是楼层', res.activeFloor);
      // 切换了一个楼层啦
      let navsLength = navs.length;
      for (let i = 0; i < navsLength; i++) {
        navs[i].className = '';
      }

      if (res.activeFloor) {
        // console.log(res.activeFloor);
        navs[res.activeIndex].className = 'active';
      }
    }
  }
});

// 5秒种后删除一个楼层和一个相应的导航
setTimeout(() => {
  // 删除导航
  navsBox.removeChild(navs[3]);
  // 从实例中删除楼层
  myFloor.del(document.getElementById('f4'), () => {console.log('del');});
}, 5000);

// 10秒种后新增一个楼层和一个相应的导航
setTimeout(() => {
  // 新增导航
  navsBox.appendChild(toDom('<li><a href="#f9">F9</a></li>'));
  // 为实例新增楼层
  myFloor.add(document.getElementById('f9'), () => {console.log('add');});
}, 10000);

// 60秒种后销毁这个实例
setTimeout(() => {
  myFloor.destroy();
}, 60000);
