// 判断入参类型
let getType = (obj) => {
  return Object.prototype.toString.call(obj).replace(/(\[object )([^\]]*)(\])/, '$2');
};
let getScrollTop = () => {return document.documentElement.scrollTop || document.body.scrollTop;};
// 判断滚动方向和是否到顶部或底部
let rect = function (ele) {
  let inHeight = window.innerHeight;
  let rect = ele.getBoundingClientRect();
  rect.isEnd = rect.bottom - inHeight <= 0;
  return rect;
};
// 兼容性处理
let RAF = (function () {return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (callback) {setTimeout(callback, 1000 / 60);};})();

/**
 * mc-floor JavaScript Library v1.0.0
 *
 * 此插件功能是通天塔层楼滚动到可见区域时发送埋点。
 *
 * @param  param            {Object}    入参
 * @param  param.el         {String}    需要监听的DOM节点
 * @param  param.offset     {String}    偏移量值(0-1)
 * @param  param.on         {Object}    事件
 * @param  param.on.up      {Function}  当向上滚动时触发
 * @param  param.on.down    {Function}  当向下滚动时触发
 * @param  param.on.top     {Function}  滚动到顶部时触发
 * @param  param.on.down    {Function}  滚动到底部时触发
 * @param  param.on.change  {Function}  楼层切换时触发
 *
 * 文档：https://gitlab.com/fekits/mc-floor
 *
 * 版本：v1.0.0
 *
 * 作者：xiaojunbo
 *
 * 兼容：IE8+
 *
 */

let McFloor = function (param) {
  if (param) {
    // 获取窗口高度
    let winHeight = window.innerHeight;
    let fun = () => {};
    let tOffset = param.topOffset;
    let bOffset = param.bottomOffset;
    this.topOffset = tOffset ? (tOffset < 1 && tOffset > 0 ? winHeight * tOffset : tOffset) : 0;
    this.bottomOffset = bOffset ? (bOffset < 1 && bOffset > 0 ? winHeight - (winHeight * bOffset) : winHeight - bOffset) : winHeight;
    this.activeFloor = null;
    this.activeIndex = null;
    this.keepActive = param.keepActive;
    this.once = param.once;
    this.free = 1;
    if (this.once) {
      this.keepActive = false;
    }
    this.on = Object.assign({ up: fun, down: fun, end: fun, top: fun, init: fun, change: fun }, param.on || {});

    // 判断指定楼层元素的入参类型并自动获取DOM元素
    if (getType(param.el) === 'Array') {
      this.floors = param.el;
    } else if (getType(param.el) === 'NodeList') {
      this.floors = Array.prototype.slice.call(param.el);
    } else if (getType(param.el) === 'HTMLDivElement') {
      this.floors = [param.el];
    } else if (getType(param.el) === 'String') {
      this.floors = Array.prototype.slice.call(document.querySelectorAll(param.el)) || [];
    } else {
      this.floors = [];
    }

    // 存储状态
    // 当前是否正在执行一个核心任务
    let previousScrollTop = getScrollTop();
    // 任务排队标识
    let tasking = 1;
    let notInit = 0;
    let previousFloor = null;
    let nearestFloor = { floor: this.floors[0], index: 0 };
    // 遍历楼层是否有当前的
    let hasActive = 0;
    // 核心代码
    let core = () => {
      // 标识为任务进行中
      tasking = 0;
      // 获取滚动距离
      let scrollTop = getScrollTop();

      if (!this.once) {
        hasActive = 0;
      }

      this.floors.map((item, index) => {
        let itemRect = item.getBoundingClientRect();
        let isChange = itemRect.top < this.bottomOffset && itemRect.bottom > this.topOffset;

        if (isChange && !item.isActived) {
          this.activeFloor = item;
          this.activeIndex = index;
          hasActive = 1;

          // 是否仅执行一次
          // console.log(97, this.once);
          if (this.once) {
            // console.log('仅一次');
            item.isActived = 1;
          }
        }
      });

      // console.log(this.floors);

      if (hasActive) {
        // console.log('当前有');

        if (!notInit || this.activeFloor !== previousFloor) {
          // 初始化过了
          notInit = 1;
          this.on.change(this);
          // previousIndex = this.activeIndex;
          previousFloor = this.activeFloor;
        }
      } else {
        if (!this.keepActive) {
          // console.log('当前无');
          this.activeFloor = null;
          this.activeIndex = null;
          if (!notInit || this.activeFloor !== previousFloor) {
            // 初始化过了
            notInit = 1;
            this.on.change(this);
            // previousIndex = this.activeIndex;
            previousFloor = this.activeFloor;
          }
        } else {
          // console.log('最近值');
          this.floors.map((item, index) => {
            let itemRect = item.getBoundingClientRect();
            if (Math.abs(itemRect.top) < Math.abs(nearestFloor.floor.getBoundingClientRect().top)) {
              nearestFloor.floor = item;
              nearestFloor.index = index;
            }
          });
          // console.log(nearestFloor);
          this.activeFloor = nearestFloor.floor;
          this.activeIndex = nearestFloor.index;
          if (!notInit || this.activeFloor !== previousFloor) {
            // 初始化过了
            notInit = 1;
            this.on.change(this);
            // previousIndex = this.activeIndex;
            previousFloor = this.activeFloor;
          }
        }

      }

      // 回调函数
      if (previousScrollTop > scrollTop) {
        this.scrollStatus = 'up';
        this.on.up(this);
      } else if (previousScrollTop < scrollTop) {
        this.scrollStatus = 'down';
        this.on.down(this);
      }

      if (scrollTop <= 0) {
        this.scrollStatus = 'top';
        this.on.top(this);
      }
      if (rect(document.body).isEnd) {
        this.scrollStatus = 'end';
        this.on.end(this);
      }

      // 更新用于对比的值
      previousScrollTop = scrollTop;
      tasking = 1;
    };

    // 初始化
    this.on.init(this);
    // 添加一个滚动事件
    core();
    this.task = () => {
      if (tasking && this.free) {
        RAF(() => {
          core();
        });
      } else {
        return false;
      }
    };
    window.addEventListener('scroll', this.task);
  }
};

// 新增一个监听元素(未完善)
McFloor.prototype.add = function (el, cb = () => {}) {
  if (el) {
    this.floors.push(el);
    cb(el);
  }
};

// 删除一个监听元素
McFloor.prototype.del = function (el, cb = () => {}) {
  // console.log('el');
  if (el && getType(el) === 'HTMLDivElement') {
    this.floors.splice(this.floors.indexOf(el) > -1 && this.floors.indexOf(el), 1);
    cb(el);
  }
  // console.log(this.floors);
};

McFloor.prototype.destroy = function () {
  window.removeEventListener('scroll', this.task);
};

export default McFloor;
